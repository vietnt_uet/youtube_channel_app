package com.collalab.yogadaily.Common;

import android.support.v4.app.Fragment;

import com.collalab.yogadaily.Model.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 29/03/2017.
 */

public class Utils {
    public static List<Item> mFavoriteItems = new ArrayList<>();
    /* fragments[0] : more
        fragments[1] : favorite
    *   fragments[2] : home
    * fragments[3] : all video
    * fragments[4] : video - from home
    * fragments[5] : video - from favorite
     */
    public static Fragment currentFragment;
    public static boolean videoFromFavorite = false;
    public static boolean videoFromAllVideo = false;
}
