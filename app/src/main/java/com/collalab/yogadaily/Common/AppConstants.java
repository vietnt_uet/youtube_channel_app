package com.collalab.yogadaily.Common;


import com.collalab.yogadaily.Model.Item;
import com.collalab.yogadaily.YoutubeApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ravikumar on 10/20/2014.
 */
public interface AppConstants {
    public static final int SEARCH_VIDEO = 1;

    public static final String SEARCH_VIDEO_MSG = "Searching Videos";
    String SEARCH_PLAYLIST_MSG = "Searching PlayLists";
    public static final String DIALOG_TITLE = "Loading";

    public static final long NUMBER_OF_VIDEOS_RETURNED = 25;
    public static final String APP_NAME = YoutubeApplication.appName();

    // Register an API key here: https://code.google.com/apis/console
    // Note : This is the browser key instead of android key as Android key was generating Service config errors (403)
    int LOAD_PLAYLIST = 2;
    int PLAYLIST_VIDEO = 3;
    int CHANNEL_VIEW = 4;
    String SHARED_PREFS_LIST = "share_prefs_list";
    String LIST_TAG = "list_tag";

}
