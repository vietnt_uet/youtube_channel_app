package com.collalab.yogadaily.Fragment;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.collalab.yogadaily.Common.Config;
import com.collalab.yogadaily.Common.Utils;
import com.collalab.yogadaily.Model.Item;
import com.collalab.yogadaily.R;
import com.collalab.yogadaily.View.FullscreenVideoLayout;
import com.collalab.yogadaily.YoutubeApplication;
import com.collalab.yogadaily.event.EventHideBottomBar;
import com.collalab.yogadaily.event.EventShowBottomBar;
import com.collalab.yogadaily.persistence.PreferenceUtils;
import com.collalab.yogadaily.persistence.PrefsKey;
//import com.commit451.youtubeextractor.YouTubeExtractionResult;
//import com.commit451.youtubeextractor.YouTubeExtractor;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.VideoOptions;
import com.squareup.picasso.Picasso;


import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import at.huber.youtubeExtractor.VideoMeta;
import at.huber.youtubeExtractor.YouTubeExtractor;
import at.huber.youtubeExtractor.YtFile;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by sev_user on 3/29/2017.
 */

public class VideoFragment extends Fragment implements View.OnClickListener {
    Context mContext;
    Item mItem;
    FullscreenVideoLayout videoLayout;
    NativeExpressAdView mAdView;
    Uri mVideoUri;
    boolean mIsFromFavorite = false;
//    private final YouTubeExtractor mExtractor = YouTubeExtractor.create();
//    private Callback<YouTubeExtractionResult> mExtractionCallback = new Callback<YouTubeExtractionResult>() {
//        @Override
//        public void onResponse(Call<YouTubeExtractionResult> call, Response<YouTubeExtractionResult> response) {
//            bindVideoResult(response.body());
//        }
//
//        @Override
//        public void onFailure(Call<YouTubeExtractionResult> call, Throwable t) {
//            onError(t);
//        }
//    };

    public VideoFragment() {

    }

    public static VideoFragment newInstance(Item item, boolean fromFavorite) {
        VideoFragment fragment = new VideoFragment();
        Bundle args = new Bundle();
        args.putSerializable("item", item);
        args.putBoolean("favorite", fromFavorite);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mItem = (Item) getArguments().getSerializable("item");
            mIsFromFavorite = getArguments().getBoolean("favorite");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        PreferenceUtils.init(getContext());
        View view = inflater.inflate(R.layout.video_fragment, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayout llVideoView = (LinearLayout) view.findViewById(R.id.ll_video_view);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.height = mContext.getResources().getDisplayMetrics().widthPixels * 9 /16;
        llVideoView.setLayoutParams(layoutParams);
        videoLayout = (FullscreenVideoLayout) view.findViewById(R.id.videoview);
        videoLayout.setActivity(getActivity());
        YoutubeApplication.isPlayingVideo = true;
//        mExtractor.extract(mItem.getId()).enqueue(mExtractionCallback);

        String youtubeLink = "https://www.youtube.com/watch?v="+ mItem.getId();
        getYoutubeDownloadUrl(youtubeLink);
        videoLayout.setShouldAutoplay(true);

        RelativeLayout adsContainer = (RelativeLayout) view.findViewById(R.id.ads_container);
        adsContainer.removeAllViews();
        mAdView = new NativeExpressAdView(mContext);
        int adsWidth = (int) ( mContext.getResources().getDisplayMetrics().widthPixels / mContext.getResources().getDisplayMetrics().density);
        mAdView.setAdSize(new AdSize(adsWidth, 250));
        mAdView.setAdUnitId(mContext.getResources().getString(R.string.ad_unit));

//        mAdView = (NativeExpressAdView) view.findViewById(R.id.adView);
        mAdView.setVideoOptions(new VideoOptions.Builder()
                .setStartMuted(true)
                .build());
        adsContainer.addView(mAdView);
        mAdView.loadAd(YoutubeApplication.sAdRequest);

        TextView title = (TextView) view.findViewById(R.id.title);
        ImageView imgShareVia = (ImageView) view.findViewById(R.id.img_share_via);
        ImageView imgFavorite = (ImageView) view.findViewById(R.id.img_favorite);
        ImageView backImg = (ImageView) view.findViewById(R.id.img_all_view_back);
        title.setText(mItem.getTitle());
        imgFavorite.setOnClickListener(this);
        backImg.setOnClickListener(this);
        imgShareVia.setOnClickListener(this);
    }

    private void getYoutubeDownloadUrl(String youtubeLink) {
        new YouTubeExtractor(mContext) {

            @Override
            public void onExtractionComplete(SparseArray<YtFile> ytFiles, VideoMeta vMeta) {

                if (ytFiles == null) {
                    // Something went wrong we got no urls. Always check this.
//                    mContext.finish();
                    getActivity().onBackPressed();
                    return;
                }
                // Iterate over itags
                for (int i = 0, itag; i < ytFiles.size(); i++) {
                    itag = ytFiles.keyAt(i);
                    // ytFile represents one file with its url and meta data
                    YtFile ytFile = ytFiles.get(itag);

                    // Just add videos in a decent format => height -1 = audio
                    if (ytFile.getFormat().getHeight() == 360) {
                        playVideo(ytFile);
                        break;
                    }
                }
            }
        }.extract(youtubeLink, true, false);
    }

    private void playVideo(YtFile ytFile){
        mVideoUri = Uri.parse(ytFile.getUrl());
        try {
            if (mVideoUri != null) {
                videoLayout.setVideoURI(mVideoUri);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void onError(Throwable t) {
        t.printStackTrace();
        Toast.makeText(mContext, "It failed to extract. So sad", Toast.LENGTH_SHORT).show();
    }


//    private void bindVideoResult(YouTubeExtractionResult result) {
//
////        Here you can get ic_share url link
//
//        mVideoUri = result.getSd360VideoUri();
//        Log.e("OnSuccess", "Got a result with the best url: " + result.getSd360VideoUri());
//        try {
//            if(mVideoUri != null){
//                videoLayout.setVideoURI(mVideoUri);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        videoLayout.resize();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().post(new EventShowBottomBar());
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(!isVisibleToUser) {
            videoLayout.pause();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_all_view_back:
                getActivity().onBackPressed();
                break;
            case R.id.img_share_via:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, mItem.getTitle());
                String url = "https://www.youtube.com/watch?v=" + mItem.getId();
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, mItem.getTitle() + "\n" + url);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
            case R.id.img_favorite:
                if (Utils.mFavoriteItems != null) {
                    for (Item item : Utils.mFavoriteItems) {
                        if (item.getId().equals(mItem.getId())) {
                            Utils.mFavoriteItems.remove(mItem);
                            break;
                        }
                    }
                }
                Utils.mFavoriteItems.add(0, mItem);
                showAddFavoriteSuccess();
                break;
        }
    }

    private void showAddFavoriteSuccess() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle(getResources().getString(R.string.dialog_title));
        alertDialogBuilder
                .setMessage(getResources().getString(R.string.dialog_message))
                .setCancelable(false)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
