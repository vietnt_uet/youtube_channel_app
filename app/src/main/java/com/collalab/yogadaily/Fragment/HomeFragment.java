package com.collalab.yogadaily.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.collalab.yogadaily.Action.ServerResponseListener;
import com.collalab.yogadaily.Action.ServiceTask;
import com.collalab.yogadaily.Adapter.PlaylistAdapter;
import com.collalab.yogadaily.Adapter.VideoAdapter;
import com.collalab.yogadaily.Common.Config;
import com.collalab.yogadaily.Model.ChannelView;
import com.collalab.yogadaily.Model.Item;
import com.collalab.yogadaily.R;
import com.collalab.yogadaily.View.ItemOffsetDecoration;
import com.google.api.services.youtube.model.Playlist;
import com.google.api.services.youtube.model.PlaylistListResponse;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ServerResponseListener, View.OnClickListener {
    private GridLayoutManager mLayoutManager;
    RecyclerView mRecyclerView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    PlaylistAdapter mPlaylistAdapter;
    private ServiceTask mYtServiceTask = null;
    private ProgressDialog mLoadingDialog = null;
    private String nextPageTokenPlaylist = "";
    List<Item> itemList = new ArrayList<>();
    ChannelView channelView = new ChannelView();
    Context mContext;

    public HomeFragment() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLayoutManager = new GridLayoutManager(mContext, 2);
        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0 || position > itemList.size()) {
                    return 2;
                }
                return 1;
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.home_swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_home);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(mContext, R.dimen.item_offset);
        mRecyclerView.addItemDecoration(itemDecoration);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    loadPlayList();
                    if (channelView == null) {
                        loadChannelView();
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                boolean enabled = mLayoutManager.findFirstCompletelyVisibleItemPosition() == 0;
                mSwipeRefreshLayout.setEnabled(enabled);
            }
        });

        loadChannelView();
        loadPlayList();
    }

    private void loadPlayList() {
        if (nextPageTokenPlaylist != null) {
            mYtServiceTask = new ServiceTask(LOAD_PLAYLIST);
            mYtServiceTask.setmServerResponseListener(this);
            Object[] params = new Object[1];
            params[0] = nextPageTokenPlaylist;
            mYtServiceTask.execute(params);
        }
    }

    private void loadChannelView() {
        mYtServiceTask = new ServiceTask(CHANNEL_VIEW);
        mYtServiceTask.setmServerResponseListener(this);
        Object[] params = new Object[1];
        params[0] = Config.CHANNEL_ID;
        mYtServiceTask.execute(params);
    }

    @Override
    public void onRefresh() {
        nextPageTokenPlaylist = "";
        loadPlayList();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void prepareRequest(Object... objects) {
        Integer reqCode = (Integer) objects[0];

        if (reqCode == null || reqCode == 0)
            throw new NullPointerException("Request Code's value is Invalid.");
        String dialogMsg = null;
        switch (reqCode) {
            case CHANNEL_VIEW:
                dialogMsg = SEARCH_VIDEO_MSG;
                break;
            case LOAD_PLAYLIST:
                dialogMsg = SEARCH_PLAYLIST_MSG;
                break;
        }

        if (mLoadingDialog != null && !mLoadingDialog.isShowing())
            mLoadingDialog = ProgressDialog.show(mContext, DIALOG_TITLE, dialogMsg, true, true);
    }

    @Override
    public void goBackground(Object... objects) {

    }

    @Override
    public void completedRequest(Object... objects) {
        if (mLoadingDialog != null && mLoadingDialog.isShowing())
            mLoadingDialog.dismiss();

        // Parse the response based upon type of request
        Integer reqCode = (Integer) objects[0];

        if (reqCode == null || reqCode == 0)
            throw new NullPointerException("Request Code's value is Invalid.");

        switch (reqCode) {
            case CHANNEL_VIEW:
                channelView = (ChannelView) objects[1];

                break;
            case LOAD_PLAYLIST:
                PlaylistListResponse playlistResponse = (PlaylistListResponse) objects[1];
                if (playlistResponse != null) {
                    List<Playlist> playlistResults = playlistResponse.getItems();
                    for (Playlist playlistResult : playlistResults) {
                        Item item = new Item();
                        item.setId(playlistResult.getId());
                        item.setDescription(playlistResult.getSnippet().getDescription());
                        item.setTitle(playlistResult.getSnippet().getTitle());
                        item.setUrlImage(playlistResult.getSnippet().getThumbnails().getMedium().getUrl());
                        item.setVideoCount(playlistResult.getContentDetails().getItemCount());
                        itemList.add(item);
                    }
                    nextPageTokenPlaylist = playlistResponse.getNextPageToken();
                    if (mPlaylistAdapter == null) {
                        mPlaylistAdapter = new PlaylistAdapter(mContext, itemList);
                        mPlaylistAdapter.setOnItemClick(mOnItemClick);
                        mRecyclerView.setAdapter(mPlaylistAdapter);
                    } else {
                        Log.e("qanh", "notifyDataSetChanged home");
                        mPlaylistAdapter.notifyDataSetChanged();
                    }
                    if (mPlaylistAdapter != null) {
                        if (nextPageTokenPlaylist == null) {
                            mPlaylistAdapter.hideFooterSpace();
                        }
                        if (channelView != null) {
                            mPlaylistAdapter.setHeaderData(channelView.getUploadsId(), channelView.getImageSettings().getBannerImageUrl(),
                                    channelView.getVideoCount(), getActivity());
                        }
                    }
                } else {
                    //No internet
                }
                break;

        }
    }

    VideoAdapter.OnItemClick mOnItemClick = new VideoAdapter.OnItemClick() {
        @Override
        public void onItemClick(int position) {
            FragmentManager mFragmentManager = getActivity().getSupportFragmentManager();
            AllVideoFragment allVideoFragment = AllVideoFragment.newInstance(itemList.get(position).getId(), itemList.get(position).getTitle());
            FragmentTransaction transaction = mFragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit, R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit);
            transaction.replace(R.id.content_home, allVideoFragment).addToBackStack(null);
            transaction.commit();
        }
    };

    @Override
    public void onClick(View v) {

    }
}
