package com.collalab.yogadaily.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.collalab.yogadaily.Adapter.VideoAdapter;
import com.collalab.yogadaily.Common.Utils;
import com.collalab.yogadaily.MainActivity;
import com.collalab.yogadaily.R;
import com.collalab.yogadaily.View.ItemOffsetDecoration;
import com.collalab.yogadaily.YoutubeApplication;
import com.collalab.yogadaily.event.EventHideBottomBar;
import com.collalab.yogadaily.event.EventPlayVideo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Administrator on 27/03/2017.
 */

public class FavoriteFragment extends Fragment {
    private LinearLayoutManager mLayoutManager;
    RecyclerView mRecyclerView;
    VideoAdapter mVideoAdapter;
    Context mContext;
    boolean isClickEdit = false;

    public FavoriteFragment() {

    }

    public static FavoriteFragment newInstance() {
        FavoriteFragment fragment = new FavoriteFragment();
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.favorite_fragment, container, false);
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(EventPlayVideo eventPlayVideo) {
        if ("all_video".equalsIgnoreCase(eventPlayVideo.data_source) && getActivity().getSupportFragmentManager().findFragmentByTag("favorite_video") != null) {
            getActivity().getSupportFragmentManager().beginTransaction().remove(getActivity().getSupportFragmentManager().findFragmentByTag("favorite_video")).commit();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && mVideoAdapter != null) {
            mVideoAdapter.notifyDataSetChanged();
        }
//        if(!isVisibleToUser) {
//            FragmentManager mFragmentManager = getChildFragmentManager();
//            if(mFragmentManager.findFragmentByTag("favorite_video") != null) {
//                mFragmentManager.beginTransaction().remove(mFragmentManager.findFragmentByTag("favorite_video")).commit();
//            }
//        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mLayoutManager = new LinearLayoutManager(mContext);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_favorite);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(mContext, R.dimen.item_offset);
        mRecyclerView.addItemDecoration(itemDecoration);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mVideoAdapter = new VideoAdapter(mContext, Utils.mFavoriteItems, true);
        mVideoAdapter.setOnItemClick(mOnItemClick);
        mRecyclerView.setAdapter(mVideoAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (mVideoAdapter != null)
                        mVideoAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        mVideoAdapter.hideFooterSpace();
        final TextView edit = (TextView) view.findViewById(R.id.edit_txt);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit.getText().equals("EDIT")) {
                    mVideoAdapter.setEditMode(true);
                    edit.setText("DONE");
                } else {
                    mVideoAdapter.setEditMode(false);
                    edit.setText("EDIT");
                }
                mVideoAdapter.notifyDataSetChanged();

            }
        });
    }

    VideoAdapter.OnItemClick mOnItemClick = new VideoAdapter.OnItemClick() {
        @Override
        public void onItemClick(int position) {
            if (YoutubeApplication.isPlayingVideo) {
                EventBus.getDefault().post(new EventPlayVideo("favorite_video"));
            }
            VideoFragment videoFragment = VideoFragment.newInstance(Utils.mFavoriteItems.get(position), true);
            getActivity().getSupportFragmentManager().beginTransaction()
            .setCustomAnimations(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit,R.anim.fragment_slide_right_enter,R.anim.fragment_slide_left_exit)
                    .replace(R.id.favorite_fragment_container, videoFragment, "favorite_video")
                    .addToBackStack(null).commit();
            EventBus.getDefault().post(new EventHideBottomBar());
        }
    };

    public void changeList() {
        mVideoAdapter.notifyDataSetChanged();
    }

    public boolean isClickEdit() {
        return isClickEdit;
    }
}
