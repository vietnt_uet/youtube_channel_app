package com.collalab.yogadaily.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.collalab.yogadaily.Action.ServerResponseListener;
import com.collalab.yogadaily.Action.ServiceTask;
import com.collalab.yogadaily.Adapter.PlaylistAdapter;
import com.collalab.yogadaily.Adapter.VideoAdapter;
import com.collalab.yogadaily.Common.Utils;
import com.collalab.yogadaily.MainActivity;
import com.collalab.yogadaily.Model.ChannelView;
import com.collalab.yogadaily.Model.Item;
import com.collalab.yogadaily.R;
import com.collalab.yogadaily.View.ItemOffsetDecoration;
import com.collalab.yogadaily.YoutubeApplication;
import com.collalab.yogadaily.event.EventAdLoaded;
import com.collalab.yogadaily.event.EventHideBottomBar;
import com.collalab.yogadaily.event.EventPlayVideo;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.PlaylistItem;
import com.google.api.services.youtube.model.PlaylistItemListResponse;
import com.thefinestartist.ytpa.YouTubePlayerActivity;
import com.thefinestartist.ytpa.enums.Orientation;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 29/03/2017.
 */

public class AllVideoFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ServerResponseListener, View.OnClickListener {
    private LinearLayoutManager mLayoutManager;
    RecyclerView mRecyclerView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    VideoAdapter mVideoAdapter;
    private ServiceTask mYtServiceTask = null;
    private ProgressDialog mLoadingDialog = null;
    private String nextPageTokenVideo = "";
    List<Item> itemList = new ArrayList<>();
    static String mUploadsId = "";
    CardView cardViewVideo;
    ImageView backImg;
    Context mContext;
    static String mPlaylistId;
    static boolean mIsAllVideo = false;
    String playListId = "";
    static String mTitle = "All Videos";

    public AllVideoFragment() {

    }

    public static AllVideoFragment newInstance(String uploadsId) {
        AllVideoFragment fragment = new AllVideoFragment();
        mUploadsId = uploadsId;
        mIsAllVideo = true;
        return fragment;
    }

    public static AllVideoFragment newInstance(String playlistId, String title) {
        AllVideoFragment fragment = new AllVideoFragment();
        mPlaylistId = playlistId;
        mIsAllVideo = false;
        mTitle = title;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.all_video_fragment, container, false);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView title = (TextView) view.findViewById(R.id.title);

        if (mIsAllVideo) {
            title.setText("All videos");
            playListId = mUploadsId;
        } else {
            title.setText(mTitle);
            playListId = mPlaylistId;
        }
        backImg = (ImageView) view.findViewById(R.id.img_all_view_back);
        backImg.setOnClickListener(this);
        mLayoutManager = new LinearLayoutManager(mContext);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.all_video_swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_all_video);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(mContext, R.dimen.item_offset);
        mRecyclerView.addItemDecoration(itemDecoration);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
//                    checkLoadMore();
                    if(mVideoAdapter != null && itemList.size() > 0){
                        if((itemList.size() - 2) < mLayoutManager.findLastCompletelyVisibleItemPosition()){
                            loadAllVideo(playListId);
                        }
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                boolean enabled = mLayoutManager.findFirstCompletelyVisibleItemPosition() == 0;
                mSwipeRefreshLayout.setEnabled(enabled);
            }
        });
        loadAllVideo(playListId);
    }

    private void loadAllVideo(String playlistId) {
        if (nextPageTokenVideo != null) {
            mYtServiceTask = new ServiceTask(PLAYLIST_VIDEO);
            mYtServiceTask.setmServerResponseListener(this);
            Object[] params = new Object[2];
            params[0] = nextPageTokenVideo;
            params[1] = playlistId;
            mYtServiceTask.execute(params);
        }
    }

    @Override
    public void onRefresh() {
        nextPageTokenVideo = "";
        loadAllVideo(playListId);
    }

    @Override
    public void prepareRequest(Object... objects) {
        Integer reqCode = (Integer) objects[0];

        if (reqCode == null || reqCode == 0)
            throw new NullPointerException("Request Code's value is Invalid.");
        String dialogMsg = SEARCH_VIDEO_MSG;
        if (mLoadingDialog != null && !mLoadingDialog.isShowing())
            mLoadingDialog = ProgressDialog.show(mContext, DIALOG_TITLE, dialogMsg, true, true);
    }

    @Override
    public void goBackground(Object... objects) {

    }

    @Override
    public void completedRequest(Object... objects) {
        if (mLoadingDialog != null && mLoadingDialog.isShowing())
            mLoadingDialog.dismiss();
        // Parse the response based upon type of request
        if(itemList != null && itemList.size() > 0) {
            if(mSwipeRefreshLayout.isRefreshing()) {
                int size = itemList.size();
                itemList.clear();
                if(mVideoAdapter != null) {
                    mVideoAdapter.notifyItemRangeRemoved(0,size);
                }
                mSwipeRefreshLayout.setRefreshing(false);
            } else {
                int size = itemList.size();
                for(int i = size - 1; i >=0; i--) {
                    if(itemList.get(i).isAdCell()) {
                        itemList.remove(i);
                        mVideoAdapter.notifyItemRemoved(i);
                    }
                }
            }
        }


        Integer reqCode = (Integer) objects[0];
        if (reqCode == null || reqCode == 0)
            throw new NullPointerException("Request Code's value is Invalid.");

        PlaylistItemListResponse playlistItemListResponse = (PlaylistItemListResponse) objects[1];
        if (playlistItemListResponse != null) {
            List<PlaylistItem> playlistItems = playlistItemListResponse.getItems();
            for (PlaylistItem playlistResult : playlistItems) {
                Item item = new Item();
                item.setId(playlistResult.getSnippet().getResourceId().getVideoId());
                item.setDescription(playlistResult.getSnippet().getDescription());
                item.setTitle(playlistResult.getSnippet().getTitle());
                if (playlistResult.getSnippet().getThumbnails() != null)
                    item.setUrlImage(playlistResult.getSnippet().getThumbnails().getMedium().getUrl());
                itemList.add(item);
            }


            if (itemList.size() > 5) {
                initAdItem();
            }

            nextPageTokenVideo = playlistItemListResponse.getNextPageToken();
            if (mVideoAdapter == null) {
                mVideoAdapter = new VideoAdapter(mContext, itemList, false);
                mVideoAdapter.setOnItemClick(mOnItemClick);
                mRecyclerView.setAdapter(mVideoAdapter);
            } else {
                Log.e("qanh", "notifyDataSetChanged all video");

                mVideoAdapter.notifyDataSetChanged();
            }
            if (nextPageTokenVideo == null) {
                mVideoAdapter.hideFooterSpace();
            }
        }

    }

    private void initAdItem() {
        int size = itemList.size();
        for (int i = size - 1; i >= 0; i--) {
            if (i % 5 == 0 && i != 0) {
                Item item = new Item();
                item.setAdCell(true);
                itemList.add(i, item);
            }
        }
    }

    @Subscribe
    public void onEvent(EventPlayVideo eventPlayVideo) {
        if ("favorite_video".equalsIgnoreCase(eventPlayVideo.data_source) && getActivity().getSupportFragmentManager().findFragmentByTag("all_video") != null) {
            getActivity().getSupportFragmentManager().beginTransaction().remove(getActivity().getSupportFragmentManager().findFragmentByTag("all_video")).commit();
        }
    }

    VideoAdapter.OnItemClick mOnItemClick = new VideoAdapter.OnItemClick() {
        @Override
        public void onItemClick(int position) {
            if (YoutubeApplication.isPlayingVideo) {
                EventBus.getDefault().post(new EventPlayVideo("all_video"));
            }
            VideoFragment videoFragment = VideoFragment.newInstance(itemList.get(position), true);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit,R.anim.fragment_slide_right_enter,R.anim.fragment_slide_right_exit)
                    .replace(R.id.all_video_fragment_container, videoFragment, "all_video")
                    .addToBackStack(null).commit();
            EventBus.getDefault().post(new EventHideBottomBar());
        }
    };

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
//        if (!isVisibleToUser) {
//            FragmentManager mFragmentManager = getChildFragmentManager();
//            if (mFragmentManager.findFragmentByTag("all_video") != null) {
//                mFragmentManager.beginTransaction().remove(mFragmentManager.findFragmentByTag("all_video")).commit();
//            }
//        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_all_view_back:
                getActivity().onBackPressed();
                break;
        }
    }
}
