package com.collalab.yogadaily.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.collalab.yogadaily.Common.Config;
import com.collalab.yogadaily.R;
import com.collalab.yogadaily.persistence.PreferenceUtils;
import com.collalab.yogadaily.persistence.PrefsKey;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MoreFragment extends Fragment {

    @BindView(R.id.btn_in_line)
    RadioButton btnInLine;
    @BindView(R.id.btn_full_screen)
    RadioButton btnFullScreen;

    @BindView(R.id.player_state_group)
    RadioGroup playerStateGroup;

    public MoreFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.more_fragment, container, false);
        ButterKnife.bind(this, view);
        PreferenceUtils.init(getContext());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        boolean isInLine = PreferenceUtils.getBoolean(PrefsKey.KEY_IS_IN_LINE_PLAYER, true);
        if (isInLine) {
            btnInLine.setChecked(true);
            btnFullScreen.setChecked(false);
        } else {
            btnInLine.setChecked(false);
            btnFullScreen.setChecked(true);
        }

        playerStateGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.btn_in_line) {
                    btnInLine.setChecked(true);
                    btnFullScreen.setChecked(false);
                    PreferenceUtils.commitBoolean(PrefsKey.KEY_IS_IN_LINE_PLAYER, true);
                } else {
                    btnInLine.setChecked(false);
                    btnFullScreen.setChecked(true);
                    PreferenceUtils.commitBoolean(PrefsKey.KEY_IS_IN_LINE_PLAYER, false);
                }
            }
        });
    }

    @OnClick(R.id.btn_rate_review)
    public void onRateReviewClick() {
        String appPackage = getContext().getPackageName();
        String url = "https://play.google.com/store/apps/details?id=" + appPackage;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    @OnClick(R.id.btn_share_app)
    public void onShareAppClick() {
        String appPackage = getContext().getPackageName();
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out this cool app about Yoga at: https://play.google.com/store/apps/details?id=" + appPackage);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @OnClick(R.id.btn_more_app)
    public void onMoreAppsClick() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(Config.APP_STORE_URL));
        startActivity(intent);
    }

    @OnClick(R.id.btn_contact_us)
    public void onContactUsClick() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", Config.AUTHOR_EMAIL, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Please type subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Please input what you want here");
        startActivity(Intent.createChooser(emailIntent, "Contact developer"));
    }
}
