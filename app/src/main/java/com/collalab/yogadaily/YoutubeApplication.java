package com.collalab.yogadaily;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.collalab.yogadaily.Common.Config;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;


/**
 * Created by sev_user on 3/28/2017.
 */

public class YoutubeApplication extends Application {
    private static Context sContext = null;
    public static int NOTIFICATION_ID = 0;
    public static boolean isPlayingVideo = false;
    public static AdRequest sAdRequest;
    public static NativeAppInstallAd sNativeAppInstallAd;
    public static NativeContentAd sNativeContentAd;
    public static Context getAppContext() {
        return sContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
        isPlayingVideo = false;
    }

    public static String appName() {
        return getAppContext().getString(R.string.app_name);
    }

    public static boolean isConnected() {
        ConnectivityManager
                cm = (ConnectivityManager) getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}
