package com.collalab.yogadaily.service;

import android.util.Log;

import com.collalab.yogadaily.notification.YogaMessageNotification;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import static com.collalab.yogadaily.YoutubeApplication.NOTIFICATION_ID;

/**
 * Created by laptop88 on 4/3/2017.
 */

public class YogaFirebaseService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        YogaMessageNotification.notify(this, remoteMessage.getNotification().getBody(), NOTIFICATION_ID++);
    }
}
