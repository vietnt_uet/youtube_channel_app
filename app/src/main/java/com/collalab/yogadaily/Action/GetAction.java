package com.collalab.yogadaily.Action;

import android.util.Log;

import com.collalab.yogadaily.Common.AppConstants;
import com.collalab.yogadaily.Common.Config;
import com.collalab.yogadaily.Model.ChannelView;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.ChannelListResponse;
import com.google.api.services.youtube.model.PlaylistItemListResponse;
import com.google.api.services.youtube.model.PlaylistListResponse;
import com.google.api.services.youtube.model.SearchListResponse;

import java.io.IOException;

import static com.collalab.yogadaily.Action.ServiceTaskInterface.transport;
import static com.collalab.yogadaily.Action.ServiceTaskInterface.jsonFactory;

public class GetAction {
    YouTube youtube;
    public GetAction(){
         youtube = new YouTube.Builder(transport, jsonFactory, new HttpRequestInitializer() {
            public void initialize(HttpRequest request) throws IOException {
            }
        }).setApplicationName(AppConstants.APP_NAME).build();
    }
    public PlaylistItemListResponse loadVideosFromPlaylist(String nextPageToken, String playlistID) {
        try {
            Log.e("qanh", "count :" + playlistID);
            YouTube.PlaylistItems.List list = youtube.playlistItems().list("id,snippet");
            list.setKey(Config.API_KEY);
            list.setPlaylistId(playlistID);
//            list.setFields("items(id/kind,id/videoId,snippet/title,snippet/description,snippet/thumbnails/default/url,snippet/thumbnails/medium/url)");
            list.setMaxResults(AppConstants.NUMBER_OF_VIDEOS_RETURNED);
            list.setPageToken(nextPageToken);
            // Call the API and print results.
            PlaylistItemListResponse playlistItemListResponse = list.execute();
            Log.e("qanh", "count :" + playlistItemListResponse);
            if (playlistItemListResponse != null) {
                return playlistItemListResponse;
            }
        } catch (GoogleJsonResponseException e) {
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
        } catch (IOException e) {
            System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
        } catch (Throwable t) {
            t.printStackTrace();
        }

        return null;
    }

    public ChannelView loadChannelView(){
        Log.e("qanh","loadChannelView");
        try {
            YouTube.Channels.List list = youtube.channels().list("id,statistics,snippet,contentDetails,brandingSettings");
            list.setKey(Config.API_KEY);
            list.setId(Config.CHANNEL_ID);
//            list.setFields("items(id/kind,id/videoId,snippet/title,snippet/description,snippet/thumbnails/default/url,snippet/thumbnails/medium/url)");
            // Call the API and print results.
            ChannelListResponse channelListResponse = list.execute();
            Log.e("qanh", "count :" + channelListResponse.getItems());
            Channel channel = channelListResponse.getItems().get(0);
            ChannelView channelView = new ChannelView();
            channelView.setVideoCount(channel.getStatistics().getVideoCount());
            channelView.setTitle(channel.getSnippet().getTitle());
            channelView.setDescription(channel.getSnippet().getDescription());
            channelView.setThumbnailDetails(channel.getSnippet().getThumbnails());
            channelView.setUploadsId(channel.getContentDetails().getRelatedPlaylists().getUploads());
            channelView.setFavoriteId(channel.getContentDetails().getRelatedPlaylists().getFavorites());
            channelView.setImageSettings(channel.getBrandingSettings().getImage());
//            if (channelView != null) {
                return channelView;
//            }
        } catch (GoogleJsonResponseException e) {
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
        } catch (IOException e) {
            System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
        } catch (Throwable t) {
            t.printStackTrace();
        }

        return null;
    }
    public PlaylistListResponse loadPlaylists(String nextPageToken) {
        try {
            // Define the API request for retrieving search results.
            YouTube.Playlists.List playlist = youtube.playlists().list("id,snippet,contentDetails");

            // Set your developer key from the Google Developers Console for
            // non-authenticated requests. See:
            // https://console.developers.google.com/
            playlist.setKey(Config.API_KEY);

            // Set chanel ID
            playlist.setChannelId(Config.CHANNEL_ID);
            playlist.setMaxResults(AppConstants.NUMBER_OF_VIDEOS_RETURNED);

            Log.e("qanh","nextToken: "+nextPageToken);
            playlist.setPageToken(nextPageToken);
            PlaylistListResponse playlistResponse = playlist.execute();
            if (playlistResponse != null) {
                return playlistResponse;
            }
        } catch (GoogleJsonResponseException e) {
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
        } catch (IOException e) {
            System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
        } catch (Throwable t) {
            t.printStackTrace();
        }

        return null;
    }

}
