package com.collalab.yogadaily.Action;

import android.os.AsyncTask;
import android.util.Log;

import com.collalab.yogadaily.Common.AppConstants;
import com.collalab.yogadaily.Common.Config;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;

import java.io.IOException;

public class ServiceTask extends AsyncTask<Object, Void, Object[]> implements
        ServiceTaskInterface {
    private static final String TAG = ServiceTask.class.getSimpleName();
    private ServerResponseListener mServerResponseListener = null;
    private int mRequestCode = 0;

    public void setmServerResponseListener(
            ServerResponseListener mServerResponseListener) {
        this.mServerResponseListener = mServerResponseListener;
    }

    public ServiceTask(int iReqCode) {
        mRequestCode = iReqCode;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mServerResponseListener.prepareRequest(mRequestCode);
    }

    @Override
    protected Object[] doInBackground(Object... params) {
        if (params == null)
            throw new NullPointerException("Parameters to the async task can never be null");

        mServerResponseListener.goBackground();

        Object[] resultDetails = new Object[2];
        resultDetails[0] = mRequestCode;
        GetAction getAction = new GetAction();
        switch (mRequestCode) {

            case AppConstants.CHANNEL_VIEW:
                resultDetails[1] = getAction.loadChannelView();
                break;
            case AppConstants.LOAD_PLAYLIST:
                resultDetails[1] = getAction.loadPlaylists((String) params[0]);
                break;
            case AppConstants.PLAYLIST_VIDEO:
                resultDetails[1] = getAction.loadVideosFromPlaylist((String) params[0],(String) params[1]);
                break;
        }

        return resultDetails;
    }

    @Override
    protected void onPostExecute(Object[] result) {
        super.onPostExecute(result);
        mServerResponseListener.completedRequest(result);
    }


    /**
     * Method
     * @see <a>https://developers.google.com/youtube/v3/code_samples/java#search_by_keyword</a>
     * @param nextPageToken
     */
    private SearchListResponse loadVideos(String nextPageToken, int typeSearch) {
        try {
            // This object is used to make YouTube Data API requests. The last
            // argument is required, but since we don't need anything
            // initialized when the HttpRequest is initialized, we override
            // the interface and provide a no-op function.
            YouTube youtube = new YouTube.Builder(transport, jsonFactory, new HttpRequestInitializer() {
                public void initialize(HttpRequest request) throws IOException {
                }
            }).setApplicationName(AppConstants.APP_NAME).build();

            // Define the API request for retrieving search results.
            YouTube.Search.List search = youtube.search().list("id,snippet");

            // Set your developer key from the Google Developers Console for
            // non-authenticated requests. See:
            // https://console.developers.google.com/
            search.setKey(Config.API_KEY);
//            search.setQ(queryTerm);
            // Restrict the search results to only include videos. See:
            // https://developers.google.com/youtube/v3/docs/search/list#type
            if(typeSearch == AppConstants.SEARCH_VIDEO){
                search.setType("video");
            }else if(typeSearch == AppConstants.LOAD_PLAYLIST){
                search.setType("playlist");
            }

            // Set chanel ID
            search.setChannelId(Config.CHANNEL_ID);
            // To increase efficiency, only retrieve the fields that the
            // application uses.
//            search.setFields("items(id/kind,id/videoId,snippet/title,snippet/description,snippet/thumbnails/default/url,snippet/thumbnails/medium/url)");
            search.setMaxResults(AppConstants.NUMBER_OF_VIDEOS_RETURNED);

            // Call the API and print results.
//            List<SearchResult> searchResultList = new ArrayList<>();
            String nextToken = "";
//            do{
            Log.e("qanh","nextToken: "+nextPageToken);
            search.setPageToken(nextPageToken);
            SearchListResponse searchResponse = search.execute();
//                List<SearchResult> item = searchResponse.getItems();
//                searchResultList.addAll(item);
//                nextPageToken = searchResponse.getNextPageToken();

//            }while (nextPageToken != null);

            if (searchResponse != null) {
                return searchResponse;
            }
        } catch (GoogleJsonResponseException e) {
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
        } catch (IOException e) {
            System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
        } catch (Throwable t) {
            t.printStackTrace();
        }

        return null;
    }

    private SearchListResponse loadPlayLists(String nextPageToken) {
        try {
            // This object is used to make YouTube Data API requests. The last
            // argument is required, but since we don't need anything
            // initialized when the HttpRequest is initialized, we override
            // the interface and provide a no-op function.
            YouTube youtube = new YouTube.Builder(transport, jsonFactory, new HttpRequestInitializer() {
                public void initialize(HttpRequest request) throws IOException {
                }
            }).setApplicationName(AppConstants.APP_NAME).build();

            // Define the API request for retrieving search results.
            YouTube.Search.List search = youtube.search().list("id,snippet");

            // Set your developer key from the Google Developers Console for
            // non-authenticated requests. See:
            // https://console.developers.google.com/
            search.setKey(Config.API_KEY);
//            search.setQ(queryTerm);
            // Restrict the search results to only include videos. See:
            // https://developers.google.com/youtube/v3/docs/search/list#type
            search.setType("playlist");
            // Set chanel ID
            search.setChannelId(Config.CHANNEL_ID);
            // To increase efficiency, only retrieve the fields that the
            // application uses.
//            search.setFields("items(id/kind,id/videoId,snippet/title,snippet/description,snippet/thumbnails/default/url,snippet/thumbnails/medium/url)");
            search.setMaxResults(AppConstants.NUMBER_OF_VIDEOS_RETURNED);

            // Call the API and print results.
//            List<SearchResult> searchResultList = new ArrayList<>();
            String nextToken = "";
//            do{
            Log.e("qanh","nextToken: "+nextPageToken);
            search.setPageToken(nextPageToken);
            SearchListResponse searchResponse = search.execute();
//                List<SearchResult> item = searchResponse.getItems();
//                searchResultList.addAll(item);
//                nextPageToken = searchResponse.getNextPageToken();

//            }while (nextPageToken != null);

            if (searchResponse != null) {
                return searchResponse;
            }
        } catch (GoogleJsonResponseException e) {
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
        } catch (IOException e) {
            System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
        } catch (Throwable t) {
            t.printStackTrace();
        }

        return null;
    }
}
