package com.collalab.yogadaily.Action;

import com.collalab.yogadaily.Common.AppConstants;

public interface ServerResponseListener extends AppConstants {

	public void prepareRequest(Object... objects);
	public void goBackground(Object... objects);
	public void completedRequest(Object... objects);

}
