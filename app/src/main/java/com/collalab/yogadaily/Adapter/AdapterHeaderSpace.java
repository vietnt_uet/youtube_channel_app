package com.collalab.yogadaily.Adapter;

/**
 * Created by VietMac on 2017-03-17.
 */

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.collalab.yogadaily.Common.Utils;
import com.collalab.yogadaily.Fragment.AllVideoFragment;
import com.collalab.yogadaily.MainActivity;
import com.collalab.yogadaily.Model.ChannelView;
import com.collalab.yogadaily.R;
import com.squareup.picasso.Picasso;

import java.math.BigInteger;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public abstract class AdapterHeaderSpace extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected static final int TYPE_ITEM = 1;
    protected static final int TYPE_HEADER = 5;
    protected static final int TYPE_FOOTER = 10;
    protected boolean shouldHideFooter;
    protected boolean notShowFooterSpace;
    protected boolean notShowHeaderSpace;
    protected boolean shouldHideHeader;
    protected String noResultText = "";
    protected String uriImageAllVideo = "";
    protected BigInteger numVideo;
    protected String uploadsId;
    FragmentActivity activity;

    public class RecyclerFooterViewHolder extends RecyclerView.ViewHolder {
        public TextView tvFooterContent;
        public View pbLoading;
        public View footerView;

        public RecyclerFooterViewHolder(View itemView) {
            super(itemView);
            tvFooterContent = (TextView) itemView.findViewById(R.id.tv_no_result);
            pbLoading = itemView.findViewById(R.id.pb_footer_progress);
            footerView = itemView.findViewById(R.id.footer_view);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        View allVideo;
        ImageView allVideoImg;
        TextView allVideoCount;
        CardView cardView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            allVideo = itemView.findViewById(R.id.all_video);
            allVideoImg = (ImageView) itemView.findViewById(R.id.all_video_thumbnail_img);
            allVideoCount = (TextView) itemView.findViewById(R.id.all_video_count);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
        }
    }

    protected Context context;


    public AdapterHeaderSpace(Context context) {
        //super(context,0,listReviews);
        this.context = context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        if (viewType == TYPE_ITEM) {
            return inflateView(parent);
        } else if (viewType == TYPE_FOOTER) {
            final View view = LayoutInflater.from(context).inflate(R.layout.footer_loading, parent, false);
            return new RecyclerFooterViewHolder(view);
        } else {
            final View view = LayoutInflater.from(context).inflate(R.layout.layout_all_videos, parent, false);
            return new HeaderViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderRecycle, int position) {
        if (!isPositionFooter(position) && !isPositionHeader(position)) {
            onBindViewWithData(holderRecycle, position);
        } else if (isPositionFooter(position)) {
            if (shouldHideFooter) {
                RecyclerFooterViewHolder recyclerFooterViewHolder = (RecyclerFooterViewHolder) holderRecycle;
                recyclerFooterViewHolder.tvFooterContent.setText(noResultText);
                recyclerFooterViewHolder.tvFooterContent.setVisibility(View.VISIBLE);
                recyclerFooterViewHolder.pbLoading.setVisibility(View.GONE);
            } else if (isPositionFooter(position)) {
                try {
                    RecyclerFooterViewHolder recyclerFooterViewHolder = (RecyclerFooterViewHolder) holderRecycle;
                    recyclerFooterViewHolder.tvFooterContent.setVisibility(View.GONE);
                    recyclerFooterViewHolder.pbLoading.setVisibility(View.VISIBLE);
                } catch (ClassCastException e) {
                    e.printStackTrace();
                }
            }
            if (notShowFooterSpace) {
                RecyclerFooterViewHolder recyclerFooterViewHolder = (RecyclerFooterViewHolder) holderRecycle;
                recyclerFooterViewHolder.footerView.setVisibility(View.GONE);
            }
        } else {
            if (notShowHeaderSpace) {
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holderRecycle;
                headerViewHolder.allVideo.setVisibility(View.GONE);
            } else {
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holderRecycle;
                headerViewHolder.allVideoCount.setText(String.format(context.getResources().getString(R.string.video_count), numVideo));
                if (!uriImageAllVideo.isEmpty()) {
                    int imgWidth = context.getResources().getDisplayMetrics().widthPixels;
                    int imgHeight = imgWidth * 9 / 16;
                    Picasso.with(context).load(uriImageAllVideo).resize(imgWidth, imgHeight).centerCrop().into(headerViewHolder.allVideoImg);
                }
                if (activity != null) {
                    headerViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            FragmentManager mFragmentManager = activity.getSupportFragmentManager();
                            AllVideoFragment allVideoFragment = AllVideoFragment.newInstance(uploadsId);
                            FragmentTransaction transaction = mFragmentManager.beginTransaction();
                            transaction.setCustomAnimations(R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit, R.anim.fragment_slide_right_enter, R.anim.fragment_slide_right_exit);
                            transaction.replace(R.id.content_home, allVideoFragment).addToBackStack(null);
                            transaction.commit();
                        }
                    });
                }
            }
        }
    }

    public abstract void onBindViewWithData(RecyclerView.ViewHolder holderRecycle, int position);

    public abstract RecyclerView.ViewHolder inflateView(ViewGroup parent);

    public abstract int getDataLength();


    @Override
    public int getItemCount() {
        return getDataLength() + 2;
    }

    protected boolean isPositionFooter(int position) {
        return position > getDataLength();
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        } else if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    public void hideFooter() {
        shouldHideFooter = true;
    }

    public void hideFooterImmediately() {
        shouldHideFooter = true;
        notifyItemChanged(getDataLength());
    }

    public void hideFooterSpace() {
        notShowFooterSpace = true;
        notifyItemChanged(getDataLength());
    }

    public void hideFooterWithText(String noResultText) {
        this.noResultText = noResultText;
        hideFooterImmediately();
    }

    public void showFooter() {
        shouldHideFooter = false;
    }

    public void hideHeaderSpace() {
        notShowHeaderSpace = true;
        notifyItemChanged(0);
    }

    public void setHeaderData(String uploadsId, String uriImageAllVideo, BigInteger numVideo, FragmentActivity activity) {
        this.uploadsId = uploadsId;
        this.uriImageAllVideo = uriImageAllVideo;
        this.numVideo = numVideo;
        this.activity = activity;
        notifyItemChanged(0);
    }
}
