package com.collalab.yogadaily.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.collalab.yogadaily.Common.Config;
import com.collalab.yogadaily.Common.Utils;
import com.collalab.yogadaily.Model.Item;
import com.collalab.yogadaily.R;
import com.collalab.yogadaily.YoutubeApplication;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoAdapter extends AdapterFooterSpace {
    List<Item> mItems;
    Context mContext;
    boolean mFromFavorite = false;
    OnItemClick onItemClick;
    boolean isEditMode = false;

    LayoutInflater mLayoutInflater;

    public VideoAdapter(Context context, List<Item> items, boolean fromFavorite) {
        super(context);
        mItems = items;
        mContext = context;
        mFromFavorite = fromFavorite;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public interface OnItemClick {
        void onItemClick(int position);
    }

    public void setOnItemClick(OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    public void setEditMode(boolean editMode) {
        isEditMode = editMode;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.video_thumbnail_imv)
        ImageView imgThumbnail;
        @BindView(R.id.video_title_txt)
        TextView txtTitle;
        @BindView(R.id.layout_content_item)
        View contentVideoItem;
        @BindView(R.id.delete_item)
        RelativeLayout deleteItem;
        @BindView(R.id.item_view)
        RelativeLayout itemViewVideo;
        @BindView(R.id.fl_adplaceholder)
        RelativeLayout adContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onBindViewWithData(RecyclerView.ViewHolder holderRecycle, final int position) {
        ViewHolder holder = (ViewHolder) holderRecycle;
        final Item item = mItems.get(position);
        int imgWidth = mContext.getResources().getDisplayMetrics().widthPixels -
                4 * (mContext.getResources().getDimensionPixelSize(R.dimen.item_offset));
        if (item.isAdCell()) {
            if (YoutubeApplication.isConnected()) {
                holder.adContainer.setVisibility(View.VISIBLE);
//                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                layoutParams.width = imgWidth;
//                layoutParams.height = mContext.getResources().getDimensionPixelSize(R.dimen.ads_height);
//
//                holder.adContainer.setLayoutParams(layoutParams);

//                NativeExpressAdView adView = (NativeExpressAdView) mLayoutInflater.inflate(R.layout.ad_native_express_layout, null, false);
                holder.adContainer.removeAllViews();
                NativeExpressAdView adView = new NativeExpressAdView(mContext);
                int adsWidth = (int) (imgWidth / mContext.getResources().getDisplayMetrics().density);
                adView.setAdSize(new AdSize(adsWidth,250));
                adView.setAdUnitId(mContext.getResources().getString(R.string.ad_unit));

                holder.adContainer.addView(adView);
                adView.loadAd(YoutubeApplication.sAdRequest);
            } else {
                holder.adContainer.setVisibility(View.GONE);
            }
            holder.contentVideoItem.setVisibility(View.GONE);

        } else {
            holder.contentVideoItem.setVisibility(View.VISIBLE);
            holder.adContainer.setVisibility(View.GONE);
            if (mFromFavorite) {
                if (isEditMode) {
                    holder.deleteItem.setVisibility(View.VISIBLE);
                    holder.deleteItem.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Utils.mFavoriteItems.remove(position);
                            notifyDataSetChanged();
                        }
                    });
                } else {
                    holder.deleteItem.setVisibility(View.GONE);
                }
            }

            holder.txtTitle.setText(item.getTitle());
            int imgHeight = imgWidth * 9 / 16;
            Picasso.with(mContext).load(item.getUrlImage()).placeholder(R.drawable.ic_video_place_holder).resize(imgWidth, imgHeight).centerCrop().into(holder.imgThumbnail);
            holder.itemViewVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClick != null) {
                        onItemClick.onItemClick(position);
                    }
                }
            });
        }
    }


    @Override
    public RecyclerView.ViewHolder inflateView(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_video_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getDataLength() {
        return mItems != null ? mItems.size() : 0;
    }
}
