package com.collalab.yogadaily.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.collalab.yogadaily.Action.ServiceTask;
import com.collalab.yogadaily.Common.Utils;
import com.collalab.yogadaily.Fragment.AllVideoFragment;
import com.collalab.yogadaily.Model.ChannelView;
import com.collalab.yogadaily.Model.Item;
import com.collalab.yogadaily.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by sev_user on 3/28/2017.
 */

public class PlaylistAdapter extends AdapterHeaderSpace {

    List<Item> mItems;
    Context mContext;
    ChannelView mChannelView;

    VideoAdapter.OnItemClick onItemClick;

    public PlaylistAdapter(Context context, List<Item> items) {
        super(context);
        mItems = items;
//        mChannelView = channelView;
        mContext = context;
    }

    public void setOnItemClick(VideoAdapter.OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.playlist_thumbnail_img)
        ImageView imgThumbnail;
        @BindView(R.id.playlist_title_txt)
        TextView txtTitle;
        @BindView(R.id.playlist_video_count)
        TextView txtVideoCount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onBindViewWithData(RecyclerView.ViewHolder holderRecycle, final int position) {
        ViewHolder holder = (ViewHolder) holderRecycle;
        final Item item = mItems.get(position - 1);
        holder.txtTitle.setText(item.getTitle());
        holder.txtVideoCount.setText(String.format(mContext.getString(R.string.video_count), item.getVideoCount()));
        int imgWidth =( mContext.getResources().getDisplayMetrics().widthPixels -
                3*(mContext.getResources().getDimensionPixelSize(R.dimen.item_offset)))/2;
        int imgHeight = imgWidth * 9 / 16;
        Picasso.with(mContext).load(item.getUrlImage()).resize(imgWidth, imgHeight).centerCrop().into(holder.imgThumbnail);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onItemClick != null ) {
                    onItemClick.onItemClick(position - 1);
                }
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder inflateView(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public int getDataLength() {
        return mItems != null ? mItems.size() : 0;
    }
}
