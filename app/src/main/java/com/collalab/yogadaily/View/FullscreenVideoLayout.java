/**
 * Copyright (C) 2016 Toshiro Sugii
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.collalab.yogadaily.View;

import java.util.Locale;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.collalab.yogadaily.R;

public class FullscreenVideoLayout extends FullscreenVideoView
        implements View.OnClickListener, SeekBar.OnSeekBarChangeListener, MediaPlayer.OnPreparedListener,
        View.OnTouchListener {

    /**
     * Log cat TAG name
     */
    private final static String TAG = "FullscreenVideoLayout";

    /**
     * RelativeLayout that contains all control related views
     */
    protected View videoControlsView;

    /**
     * SeekBar reference (from videoControlsView)
     */
    protected SeekBar seekBar;

    /**
     * Reference to ImageButton play
     */
    protected ImageButton imgplay;

    /**
     * Reference to ImageButton fullscreen
     */
    protected ImageButton imgfullscreen;

    /**
     * Reference to TextView for elapsed time and total time
     */
    protected TextView textTotal, textElapsed;

    protected OnTouchListener touchListener;

    //    public long start_time = 0;
    //    private boolean couting = true;
    private Handler mHandler = null;
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideControls();
                }
            });
        }

    };

    /**
     * Handler and Runnable to keep tracking on elapsed time
     */
    protected static final Handler TIME_THREAD = new Handler();
    protected Runnable updateTimeRunnable = new Runnable() {
        public void run() {
            updateCounter();

            TIME_THREAD.postDelayed(this, 200);
        }
    };

    public FullscreenVideoLayout(Context context) {
        super(context);
    }

    public FullscreenVideoLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FullscreenVideoLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void initHelper(){
        if(mHandler==null)
            mHandler = new Handler();
        if (mRunnable == null)
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideControls();
                        }
                    });
                }

            };
    }

    @Override
    protected void init() {
        Log.d(TAG, "init");

        super.init();

        if (this.isInEditMode())
            return;

        // We need to add it to show/hide the controls
        super.setOnTouchListener(this);

    }

    @Override
    protected void initObjects() {
        super.initObjects();

        if (this.videoControlsView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.videoControlsView = inflater.inflate(R.layout.view_videocontrols, this, false);
        }

        if (videoControlsView != null) {
            LayoutParams params =
                    new LayoutParams(LayoutParams.FILL_PARENT,
                                                    LayoutParams.WRAP_CONTENT);
            //params.addRule(ALIGN_PARENT_BOTTOM);
            params.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.surfaceView);
            addView(videoControlsView, params);

            this.seekBar = (SeekBar) this.videoControlsView.findViewById(R.id.vcv_seekbar);
            this.imgfullscreen = (ImageButton) this.videoControlsView.findViewById(R.id.vcv_img_fullscreen);
            this.imgplay = (ImageButton) this.videoControlsView.findViewById(R.id.vcv_img_play);
            this.textTotal = (TextView) this.videoControlsView.findViewById(R.id.vcv_txt_total);
            this.textElapsed = (TextView) this.videoControlsView.findViewById(R.id.vcv_txt_elapsed);
        }

        if (this.imgplay != null)
            this.imgplay.setOnClickListener(this);
        if (this.imgfullscreen != null)
            this.imgfullscreen.setOnClickListener(this);
        if (this.seekBar != null)
            this.seekBar.setOnSeekBarChangeListener(this);

        // Start controls invisible. Make it visible when it is prepared
        if (this.videoControlsView != null)
            this.videoControlsView.setVisibility(View.INVISIBLE);
        initHelper();
        mHandler.postDelayed(mRunnable, 3000);
    }

    @Override
    protected void releaseObjects() {
        super.releaseObjects();

        if (this.videoControlsView != null)
            removeView(this.videoControlsView);
    }

    protected void startCounter() {
        Log.d(TAG, "startCounter");

        TIME_THREAD.postDelayed(updateTimeRunnable, 200);
    }

    protected void stopCounter() {
        Log.d(TAG, "stopCounter");

        TIME_THREAD.removeCallbacks(updateTimeRunnable);
    }

    protected void updateCounter() {
        if (this.textElapsed == null)
            return;

        int elapsed = getCurrentPosition();
        // getCurrentPosition is a little bit buggy :(
        if (elapsed > 0 && elapsed < getDuration()) {
            seekBar.setProgress(elapsed);

            elapsed = Math.round(elapsed / 1000.f);
            long s = elapsed % 60;
            long m = (elapsed / 60) % 60;
            long h = (elapsed / (60 * 60)) % 24;

            if (h > 0)
                textElapsed.setText(String.format(Locale.US, "%d:%02d:%02d", h, m, s));
            else
                textElapsed.setText(String.format(Locale.US, "%02d:%02d", m, s));
        }
    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {
        touchListener = l;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Log.d(TAG, "onCompletion");

        super.onCompletion(mp);
        stopCounter();
        updateControls();
        if (currentState != State.ERROR)
            updateCounter();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        boolean result = super.onError(mp, what, extra);
        stopCounter();
        updateControls();
        return result;
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (getCurrentState() == State.END) {
            Log.d(TAG, "onDetachedFromWindow END");
            stopCounter();
        }
    }

    @Override
    protected void tryToPrepare() {
        Log.d(TAG, "tryToPrepare");
        super.tryToPrepare();

        if (getCurrentState() == State.PREPARED || getCurrentState() == State.STARTED) {
            if (textElapsed != null && textTotal != null) {
                int total = getDuration();
                if (total > 0) {
                    seekBar.setMax(total);
                    seekBar.setProgress(0);

                    total = total / 1000;
                    long s = total % 60;
                    long m = (total / 60) % 60;
                    long h = (total / (60 * 60)) % 24;
                    if (h > 0) {
                        textElapsed.setText("00:00:00");
                        textTotal.setText(String.format(Locale.US, "%d:%02d:%02d", h, m, s));
                    } else {
                        textElapsed.setText("00:00");
                        textTotal.setText(String.format(Locale.US, "%02d:%02d", m, s));
                    }
                }
            }

            if (videoControlsView != null)
                videoControlsView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void start() throws IllegalStateException {
        Log.d(TAG, "start");

        if (!isPlaying()) {
            super.start();
            startCounter();
            updateControls();
        }
    }

    @Override
    public void pause() throws IllegalStateException {
        Log.d(TAG, "pause");

        if (isPlaying()) {
            stopCounter();
            super.pause();
            updateControls();
        }
    }

    @Override
    public void reset() {
        Log.d(TAG, "reset");

        super.reset();

        stopCounter();
        updateControls();
    }

    @Override
    public void stop() throws IllegalStateException {
        Log.d(TAG, "stop");

        super.stop();
        stopCounter();
        updateControls();
    }

    protected void updateControls() {
        if (imgplay == null)
            return;

        Drawable icon;
        if (getCurrentState() == State.STARTED) {
            icon = context.getResources().getDrawable(R.mipmap.ic_pause);
        } else {
            icon = context.getResources().getDrawable(R.mipmap.ic_play);
        }
        imgplay.setBackgroundDrawable(icon);
    }

    public void hideControls() {
        Log.d(TAG, "hideControls");
        if (videoControlsView != null) {
            videoControlsView.setVisibility(View.INVISIBLE);
            Log.d("anhquan","hide: "+(System.currentTimeMillis()-startTime));
        }
    }

    public void showControls() {
        Log.d(TAG, "showControls");
        if (videoControlsView != null) {
            videoControlsView.setVisibility(View.VISIBLE);

            //            start_time = System.currentTimeMillis();
        }
    }
long startTime;
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (videoControlsView != null) {
                if (videoControlsView.getVisibility() == View.VISIBLE) {
                    hideControls();
                    if(mHandler==null)
                        mHandler = new Handler();
//                    if(mRunnable==null)
//                        mRunnable = new Runnable() {
//                            @Override
//                            public void run() {
//                                activity.runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        hideControls();
//                                    }
//                                });
//                            }
//
//                        };
                    mHandler.removeCallbacks(mRunnable);
                } else {
                    showControls();
                    startTime = System.currentTimeMillis();
                    Log.d("anhquan","show: "+startTime);
                    if(mHandler==null)
                        mHandler = new Handler();
//                    if(mRunnable==null)
//                        mRunnable = new Runnable() {
//                            @Override
//                            public void run() {
//                                activity.runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        hideControls();
//                                    }
//                                });
//                            }
//
//                        };
                    mHandler.postDelayed(mRunnable, 3000);
                }
            }
        }

        if (touchListener != null) {
            return touchListener.onTouch(FullscreenVideoLayout.this, event);
        }

        return false;
    }

    /**
     * Onclick action
     * Controls play button and fullscreen button.
     *
     * @param v View defined in XML
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.vcv_img_play) {
            if (isPlaying()) {
                pause();
            } else {
                start();
            }
        } else {
            if (isFullscreen()) {
                imgfullscreen
                        .setBackgroundDrawable(context.getResources().getDrawable(R.drawable.ic_fullscreen_white_24dp));
            } else {
                imgfullscreen.setBackgroundDrawable(
                        context.getResources().getDrawable(R.drawable.ic_fullscreen_exit_white_24dp));
            }
            setFullscreen(!isFullscreen());

        }
    }

    /**
     * SeekBar Listener
     */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        Log.d(TAG, "onProgressChanged " + progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        stopCounter();
        Log.d(TAG, "onStartTrackingTouch");

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        int progress = seekBar.getProgress();
        seekTo(progress);
        Log.d(TAG, "onStopTrackingTouch");

    }

    //    private class MyAsyncTask extends AsyncTask<Void, Boolean, Void> {
    //
    //        @Override
    //        protected Void doInBackground(Void... params) {
    //
    //            while(couting){
    //                long current_time = System.currentTimeMillis();
    //                if (current_time - start_time >= 3000) {
    //                    publishProgress(true);
    //                } else {
    //                    publishProgress(false);
    //                }
    //            }
    //            return null;
    //        }
    //
    //        @Override
    //        protected void onProgressUpdate(Boolean... values) {
    //            super.onProgressUpdate(values);
    //            if(values[0]){
    //                hideControls();
    //            }
    //        }
    //    }
}
