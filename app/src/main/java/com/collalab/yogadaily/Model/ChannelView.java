package com.collalab.yogadaily.Model;

import com.google.api.services.youtube.model.ImageSettings;
import com.google.api.services.youtube.model.ThumbnailDetails;

import java.math.BigInteger;

/**
 * Created by sev_user on 3/28/2017.
 */

public class ChannelView {
    private ThumbnailDetails thumbnailDetails;
    private String description;
    private String title;
    private String uploadsId;
    private String favoriteId;
    private BigInteger videoCount;
    private ImageSettings imageSettings;

    public ImageSettings getImageSettings() {
        return imageSettings;
    }

    public void setImageSettings(ImageSettings imageSettings) {
        this.imageSettings = imageSettings;
    }

    public ThumbnailDetails getThumbnailDetails() {
        return thumbnailDetails;
    }

    public void setThumbnailDetails(ThumbnailDetails thumbnailDetails) {
        this.thumbnailDetails = thumbnailDetails;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUploadsId() {
        return uploadsId;
    }

    public void setUploadsId(String uploadsId) {
        this.uploadsId = uploadsId;
    }

    public String getFavoriteId() {
        return favoriteId;
    }

    public void setFavoriteId(String favoriteId) {
        this.favoriteId = favoriteId;
    }

    public BigInteger getVideoCount() {
        return videoCount;
    }

    public void setVideoCount(BigInteger videoCount) {
        this.videoCount = videoCount;
    }
}
