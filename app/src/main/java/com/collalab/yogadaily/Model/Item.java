package com.collalab.yogadaily.Model;

import java.io.Serializable;

public class Item implements Serializable {
    private String id;
    private String description;
    private String title;
    private Long videoCount;
    private String urlImage;
    private boolean isAdCell;

    public boolean isAdCell() {
        return isAdCell;
    }

    public void setAdCell(boolean adCell) {
        isAdCell = adCell;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public Long getVideoCount() {
        return videoCount;
    }

    public void setVideoCount(Long videoCount) {
        this.videoCount = videoCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
