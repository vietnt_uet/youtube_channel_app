package com.collalab.yogadaily;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;

import com.collalab.yogadaily.Common.Utils;
import com.collalab.yogadaily.Fragment.FavoriteFragment;
import com.collalab.yogadaily.Fragment.HomeFragment;
import com.collalab.yogadaily.Fragment.MoreFragment;
import com.collalab.yogadaily.Model.Item;
import com.collalab.yogadaily.event.EventHideBottomBar;
import com.collalab.yogadaily.event.EventShowBottomBar;
import com.devspark.appmsg.AppMsg;
import com.github.pwittchen.networkevents.library.BusWrapper;
import com.github.pwittchen.networkevents.library.ConnectivityStatus;
import com.github.pwittchen.networkevents.library.NetworkEvents;
import com.github.pwittchen.networkevents.library.event.ConnectivityChanged;
import com.google.android.gms.ads.AdRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import static com.collalab.yogadaily.Common.AppConstants.LIST_TAG;
import static com.collalab.yogadaily.Common.AppConstants.SHARED_PREFS_LIST;


public class MainActivity extends AppCompatActivity {

    private HomeFragment homeFragment;
    private FavoriteFragment favoriteFragment;
    private MoreFragment moreFragment;
    private FragmentManager mFragmentManager;
    FragmentTransaction transaction;
    static FrameLayout videoFrag;
    public Fragment fragments[] = new Fragment[3];
    BottomNavigationView navigationView;
    int bottomBarHeight = 0;
    boolean isBottomBarShow = true;
    View layoutNavigation;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    addTabFragment(2);
                    break;
                case R.id.navigation_favorite:
                    addTabFragment(1);
                    break;
                case R.id.navigation_more:
                    addTabFragment(0);
                    if(!isBottomBarShow) {
                        isBottomBarShow = true;
                        navigationView.setTranslationY(0);
                    }
                    break;
            }
            return true;
        }

    };

    private BusWrapper busWrapper;
    private NetworkEvents networkEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        homeFragment = new HomeFragment();
        favoriteFragment = FavoriteFragment.newInstance();
        moreFragment = new MoreFragment();
        mFragmentManager = getSupportFragmentManager();
        transaction = mFragmentManager.beginTransaction();

        layoutNavigation = findViewById(R.id.layout_navigation);

        final EventBus bus = new EventBus();
        busWrapper = getGreenRobotBusWrapper(bus);
        networkEvents = new NetworkEvents(this, busWrapper).enableInternetCheck();

        addTabFragment(0);
        addTabFragment(1);
        addTabFragment(2);
        navigationView = (BottomNavigationView) findViewById(R.id.navigation);
        navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        SharedPreferences prefs = getSharedPreferences(SHARED_PREFS_LIST, Context.MODE_PRIVATE);
        String jsonList = prefs.getString(LIST_TAG, "");
        Utils.mFavoriteItems = new Gson().fromJson(jsonList, new TypeToken<List<Item>>() {
        }.getType());
        if (Utils.mFavoriteItems == null) {
            Utils.mFavoriteItems = new ArrayList<>();
        }

        if(!YoutubeApplication.isConnected()) {
            AppMsg.makeText(MainActivity.this, "No internet connection!", AppMsg.STYLE_ALERT).show();
        }

        YoutubeApplication.sAdRequest = new AdRequest.Builder().build();

        getHeightBottomBar();
    }

    private void getHeightBottomBar() {
        layoutNavigation.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onGlobalLayout() {
                layoutNavigation.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                bottomBarHeight = layoutNavigation.getHeight();
            }
        });
    }

    private void addTabFragment(int id) {

        if (fragments[id] == null) {
            switch (id) {
                case 0:
                    fragments[0] = moreFragment;
                    break;
                case 1:
                    fragments[1] = favoriteFragment;
                    break;
                case 2:
                    fragments[2] = homeFragment;
                    break;
                default:
                    break;
            }
            transaction = mFragmentManager.beginTransaction();
            transaction.add(R.id.content, fragments[id]);
            transaction.commit();
            fragments[id].setUserVisibleHint(true);
        } else {
            transaction = mFragmentManager.beginTransaction();
            for (int i = 0; i < 3; i++) {
                if (i == id) {
                    transaction.show(fragments[i]);
                    fragments[i].setUserVisibleHint(true);
                } else {
                    if (fragments[i] != null) {
                        fragments[i].setUserVisibleHint(false);
                        transaction.hide(fragments[i]);
                    }
                }
            }

            transaction.commit();
        }
    }

    @Subscribe
    public void onEvent(EventShowBottomBar eventShowBottomBar) {
        if(!isBottomBarShow) {
            isBottomBarShow = true;
            layoutNavigation.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onEvent(EventHideBottomBar eventHideBottomBar) {
        if(isBottomBarShow) {
            isBottomBarShow = false;
            layoutNavigation.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        busWrapper.register(this);
        networkEvents.register();
        if(!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onEvent(ConnectivityChanged event) {
        if (event.getConnectivityStatus() == ConnectivityStatus.OFFLINE) {
            AppMsg.makeText(MainActivity.this, "No internet connection!", AppMsg.STYLE_ALERT).show();
        } else {
            AppMsg.makeText(MainActivity.this, "Internet connected!", AppMsg.STYLE_INFO).show();
        }
    }

    @NonNull
    private BusWrapper getGreenRobotBusWrapper(final EventBus bus) {
        return new BusWrapper() {
            @Override
            public void register(Object object) {
                bus.register(object);
            }

            @Override
            public void unregister(Object object) {
                bus.unregister(object);
            }

            @Override
            public void post(Object event) {
                bus.post(event);
            }
        };
    }

    @Override
    protected void onStop() {
        SharedPreferences prefs = getSharedPreferences(SHARED_PREFS_LIST, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(Utils.mFavoriteItems);
        editor.putString(LIST_TAG, json);
        editor.apply();
        EventBus.getDefault().unregister(this);
        busWrapper.unregister(this);
        networkEvents.unregister();
        super.onStop();
    }
}
